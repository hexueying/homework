import qhyccd # import qhyccd wrapper
import numpy as np
from matplotlib import pyplot as plt
import cv2
import time
from astropy.io import fits

qc = qhyccd.qhyccd() # create qhyccd object
qc.SetExposure(5) # set exposure to 1000ms

def getbias():
    pass

def getflat():
    pass

def getdark():
    pass

def getfinal():
    pass

def shotpipline():
    pass

def save_file(img):
    hdu = fits.PrimaryHDU(img)
    hdul = fits.HDUList([hdu])
    hdul.writeto('./new-'+time.strftime("%H-%M-%S")+".fits")

while 1:
    #print(1/0)
    image = qc.GetSingleFrame()
    if cv2.waitKey(1) & 0xFF == ord('o'):
        save_file(image)
    imgshow=image.copy()
    imgshow=imgshow[...,::-1]
    imgshow=cv2.resize(imgshow,(image.shape[1]//3,image.shape[0]//3))
    cv2.imshow('live',imgshow)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cv2.destroyAllWindows()
qc.close()