import random
import copy
suits = ['hearts', 'clubs', 'diamonds', 'spades'] 
number = ['2','3','4','5','6','7','8','9','10','J','Q','K','A']
card = []
number_flush = []

for i in suits:
    for j in number:
        card.append ({
            'suits': i, 
            'number': j, 
        })
        
for i in range(9):
    number_flush.append ( "".join(number[i:i+5]))
    
        
def royal_flush(hold_all):
    count = 0

    suits_count =[]
    for i in suits:
        suits_count.append ({
            'suits': i, 
            'number': [],
            'count': 0,
        })
        
    for dicti in hold_all:
        for suit in suits_count:
            if dicti['suits'] == suit['suits']:
                suit['number'].append(dicti['number'])
                suit['count'] +=1

    for suit in suits_count:       
        if suit['count'] >=5 :
            alist = []
            for value1 in number:
                for value2 in suit['number']:
                    if value1 == value2:
                        alist.append(value1)
            suit['number'] = alist
            alist_str = "".join(alist)
            for i in number_flush:
                if i in alist_str:
                    return 1 
                
    if count == 0:
        return 0

def card_generator(card):
    for i in range(52):
        yield card.pop(random.randint(0,len(card)-1)) 
          
count = 0
times = 10000

for i in range(times):
    hold_all = []
    
    card_in = copy.copy(card)
    hold = card_generator(card_in)     
    for p in range(13):
        hold_all.append(next(hold))
    
    if royal_flush(hold_all) :
        count = count + 1

print(count/times)
